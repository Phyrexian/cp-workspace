#ifndef BINARY_SEARCH_H
#define BINARY_SEARCH_H

template <class T, class Ok>
T binary_search(T l, T r, Ok ok, T eps = 1) {
    while (r - l > eps) {
        T mid = (l + r) / 2;
        if (ok(mid))
            l = mid;
        else
            r = mid;
    }
    return l;
}

#endif