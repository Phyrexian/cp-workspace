#ifndef DS_INTERVAL_TREE_H
#define DS_INTERVAL_TREE_H

#include "base.h"

struct IntervalValues {
    long long min;
    long long max;
    long long sum;
};

std::ostream& operator<<(std::ostream& os, const IntervalValues& id) {
    return os << "{min: " << id.min << ", max: " << id.max << ", sum: " << id.sum << "}";
}

class IntervalTree {   
public:
    IntervalTree(long long begin, long long end)
        : root_(new Node(begin, end)) {}

    IntervalValues get(long long begin, long long end) {
        return root_->get(begin, end);
    }

    IntervalValues get(long long index) {
        return root_->get(index, index + 1);
    }

    void set(long long begin, long long end, long long val) {
        root_->set(begin, end, val);
    }

    void set(long long index, long long val) {
        root_->set(index, index + 1, val);
    }

    void add(long long begin, long long end, long long val) {
        root_->add(begin, end, val);
    }

    void add(long long index, long long val) {
        root_->add(index, index + 1, val);
    }

private:
    class Node {
    public:
        Node(long long begin, long long end)
            : begin_(begin)
            , end_(end)
            , left_(nullptr)
            , right_(nullptr)
            , lazy_add_(0)
            , values_{0, 0, 0} {}

        void dont_be_lazy() {
            if (!left_) {
                long long mid = (begin_ + end_) / 2;
                left_ = new Node(begin_, mid);
                right_ = new Node(mid, end_);
            }

            if (lazy_set_) {
                long long val = lazy_set_.value();

                left_->lazy_set_ = val;
                left_->lazy_add_ = 0;
                left_->values_.min = val;
                left_->values_.max = val;
                left_->values_.sum = (left_->end_ - left_->begin_) * val;

                right_->lazy_set_ = val;
                right_->lazy_add_ = 0;
                right_->values_.min = val;
                right_->values_.max = val;
                right_->values_.sum = (right_->end_ - right_->begin_) * val;

                lazy_set_ = std::nullopt;
            }

            if (lazy_add_) {
                left_->lazy_add_ += lazy_add_;
                left_->values_.min += lazy_add_;
                left_->values_.max += lazy_add_;
                left_->values_.sum += (left_->end_ - left_->begin_) * lazy_add_;

                right_->lazy_add_ += lazy_add_;
                right_->values_.min += lazy_add_;
                right_->values_.max += lazy_add_;
                right_->values_.sum += (right_->end_ - right_->begin_) * lazy_add_;

                lazy_add_ = 0;
            }
        }

        void update() {
            values_.min = std::min(left_->values_.min, right_->values_.min);
            values_.max = std::max(left_->values_.max, right_->values_.max);
            values_.sum = left_->values_.sum + right_->values_.sum;
        }

        IntervalValues get(long long begin, long long end) {
            if (begin_ >= begin && end_ <= end)
                return values_;
            if (begin_ >= end || end_ <= begin)
                return {LLONG_MAX, LLONG_MIN, 0};
            
            dont_be_lazy();
            auto left_values = left_->get(begin, end);
            auto right_values = right_->get(begin, end);
            return {
                std::min(left_values.min, right_values.min),
                std::max(left_values.max, right_values.max),
                left_values.sum + right_values.sum
            };
        }

        void set(long long begin, long long end, long long val) {
            if (begin_ >= begin && end_ <= end) {
                lazy_set_ = val;
                lazy_add_ = 0;
                values_.min = val;
                values_.max = val;
                values_.sum = (end_ - begin_) * val;
                return;
            }
            if (begin_ >= end || end_ <= begin)
                return;

            dont_be_lazy();
            left_->set(begin, end, val);
            right_->set(begin, end, val);
            update();
        }

        void add(long long begin, long long end, long long val) {
            if (begin_ >= begin && end_ <= end) {
                lazy_add_ += val;
                values_.min += val;
                values_.max += val;
                values_.sum += (end_ - begin_) * val;
                return;
            }
            if (begin_ >= end || end_ <= begin)
                return;

            dont_be_lazy();
            left_->add(begin, end, val);
            right_->add(begin, end, val);
            update();
        }

    private:
        long long begin_;
        long long end_;
        Node* left_;
        Node* right_;
        std::optional<long long> lazy_set_;
        long long lazy_add_;
        IntervalValues values_;
    };

    Node* root_;
};

#endif