#ifndef PREFIX_SUMS
#define PREFIX_SUMS

#include "base.h"

template <class T>
class PrefixSums {
public:
    PrefixSums(const std::vector<T>& v) {
        p_.assign(v.size() + 1, 0);
        for (size_t i = 0; i < v.size(); ++i)
            p_[i + 1] = p_[i] + v[i];
    }

    T sum(int l, int r) {
        return p_[r] - p_[l];
    }

    friend std::string pretty(const PrefixSums<T>& ps) {
        return pretty(ps.p_);
    }

private:
    std::vector<T> p_;
};

template <class T>
class PrefixSums2 {
public:
    PrefixSums2(const std::vector<std::vector<T>>& v) {
        p_.assign(v.size() + 1, std::vector<T>(v[0].size() + 1, 0));
        for (size_t i = 0; i < v.size(); ++i)
            for (size_t j = 0; j < v[0].size(); ++j)
                p_[i + 1][j + 1] = p_[i + 1][j] + p_[i][j + 1] - p_[i][j] + v[i][j];
    }

    T sum(int x1, int y1, int x2, int y2) {
        return p_[x2][y2] - p_[x2][y1] - p_[x1][y2] + p_[x1][y1];
    }

    friend std::string pretty(const PrefixSums2<T>& ps) {
        return pretty(ps.p_);
    }

private:
    std::vector<std::vector<T>> p_;
};

#endif