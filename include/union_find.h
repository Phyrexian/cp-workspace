#ifndef DS_UNION_FIND_H
#define DS_UNION_FIND_H

#include "base.h"

class UnionFind {
public:
    UnionFind(int n) {
        parent_.resize(n);
        for (int i = 0; i < n; i++)
            parent_[i] = i;
        rank_.assign(n, 0);
        size_.assign(n, 1);
    }

    int find(int x) {
        if (parent_[x] == x)
            return x;
        return parent_[x] = find(parent_[x]);
    }

    bool unite(int x, int y) {
        x = find(x);
        y = find(y);
        if (x == y)
            return false;
        if (rank_[x] > rank_[y]) {
            parent_[y] = x;
            size_[x] += size_[y];
        } else if (rank_[x] < rank_[y]) {
            parent_[x] = y;
            size_[y] += size_[x];
        } else {
            parent_[y] = x;
            size_[x] += size_[y];
            rank_[x]++;
        }
        return true;
    }

    int size(int x) {
        return size_[find(x)];
    }

    friend std::string pretty(UnionFind uf) {
        std::map<int, std::vector<int>> m;
        for (size_t i = 0; i < uf.parent_.size(); i++)
            m[uf.find(i)].push_back(i);
        return pretty(m);
    }

private:
    std::vector<int> parent_;
    std::vector<int> rank_;
    std::vector<int> size_;
};

#endif