def get_include(line):
    if line.startswith('#include') and line.count('"') == 2:
        return line[line.index('"') + 1 : line.rindex('"')]
    return None

def resolve_includes(file_path, included=set()):
    output_file = ''
    with open(file_path, 'r') as file:
        print(f'Resolving includes {file_path}')
        while True:
            line = file.readline()
            if not line:
                break

            include = get_include(line)
            if include:
                if include not in included:
                    output_file += resolve_includes(f'include/{include}', included=included) + '\n\n'
                    included.add(include)
            else:
                output_file += line
    return output_file

submit = resolve_includes('main.cc')
with open('submit.cc', 'w') as file:
    file.write(submit)
    print('Wrote submit.cc')

